import $ from 'jquery';
import 'font-awesome/less/font-awesome.less';
import moment from 'moment';
import io from 'socket.io-client/socket.io';
import './css/style.less';


$(() => {
  const FADE_TIME = 150; // ms
  const TYPING_TIMER_LENGTH = 400; // ms
  const COLORS = [
    '#f44336', '#E91E63', '#9C27B0', '#673AB7',
    '#3F51B5', '#2196F3', '#03A9F4', '#009688',
    '#8BC34A', '#FFEB3B', '#FFC107', '#795548'
  ];

  // Initialize variables
  const $window = $(window);
  const $usernameInput = $('.usernameInput'); // Input for username
  const $messages = $('.messages'); // Messages area
  const $inputMessage = $('.inputMessage'); // Input message input box
  const $loginPage = $('.login.page'); // The login page
  const $chatPage = $('.chat.page'); // The chatroom page

  // Prompt for setting a username
  let username;
  let connected = false;
  let typing = false;
  let lastTypingTime;
  let $currentInput = $usernameInput.focus();
  let editing = false;
  let idmessage;
  const socket = io();


   // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  function addMessageElement(el, options) {
    const $el = $(el);

    // Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
  }

  // Log a message
  function log(message, options) {
    const $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }


  function addParticipantsMessage(data) {
    let message = '';
    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += `there are ${data.numUsers} participants`;
    }
    log(message);
  }

  // Prevents input from having injected markup
  function cleanInput(input) {
    return $('<div/>').text(input).text();
  }

  // Sets the client's username
  function setUsername() {
    username = cleanInput($usernameInput.val().trim());

    // If the username is valid
    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();
      $loginPage.off('click');
      $currentInput = $inputMessage.focus();

      // Tell the server your username
      socket.emit('add user', username);
    }
  }

   // Gets the 'X is typing' messages of a user
  function getTypingMessages(data) {
    return $('.typing.message').filter((i) => {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  function getUsernameColor(username) {
    // Compute hash code
    let hash = 7;
    for (let i = 0; i < username.length; i++) {
      hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    const index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  // Create body of new message
  function createMessage(data) {
    const date = moment(data.messageDate).format('H:mm');
    const $messageWrapper = $('<div class="messageWrapper"></div>');
    const $messageContent = $('<div class="messageContent"></div>');
    const $messageStatus = $('<div class="messageStatus"></div>');
    const $usernameDiv = $('<span class="username"/>')
      .text(data.username)
      .css('color', getUsernameColor(data.username));
    const $messageBodyDiv = $('<span class="messageBody">')
      .text(data.message);
    const typingClass = data.typing ? 'typing' : '';
    const selfMessage = data.username === username ? 'selfMessage' : '';
    const $messageDate = $('<span class="messageDate" />')
      .text(date);
    if (data.edited) {
      const $messageEdit = $('<span class="messageEdit" />').text('edited');
      $messageStatus.append($messageEdit);
    }
    $messageStatus.append($messageDate);
    $messageContent.append($usernameDiv, $messageBodyDiv);
    $messageWrapper.append($messageContent, $messageStatus);
    const $messageDiv = $('<li class="message"/>')
      .data('username', data.username)
      .attr('id', data.idmessage)
      .addClass(`${typingClass} ${selfMessage}`)
      .append($messageWrapper);

    return $messageDiv;
  }


  // Adds the visual chat message to the message list
  function addChatMessage(data, options) {
    if (data.deleted) {
      const $messageDiv = $('<li class="deleted"/>')
        .text(`message deleted by ${data.username}`);
      addMessageElement($messageDiv, options);
      return;
    }
    // Don't fade the message in if there is an 'X was typing'
    const $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }
    const $messageDiv = createMessage(data);

    addMessageElement($messageDiv, options);
  }


  // Sends a chat message
  function sendMessage() {
    let message = $inputMessage.val().trim();
    const date = moment.utc();
    const timestamp = date.format('x');

    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      addChatMessage({
        username,
        message,
        messageDate: date.local(),
        idmessage: timestamp,
        deleted: false,
        edited: false
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', {
        message,
        messageDate: date,
        idmessage: timestamp,
        deleted: false,
        edited: false
      });
    }
  }

  // Adds the visual chat typing message
  function addChatTyping(data) {
    data.typing = true;
    data.message = '...';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  function removeChatTyping(data) {
    getTypingMessages(data).fadeOut(() => {
      $(this).remove();
    });
  }

  // Updates the typing event
  function updateTyping() {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(() => {
        const typingTimer = (new Date()).getTime();
        const timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // create buttons for edit and delete message
  function createButtons() {
    const div = $('<div class="editMessage"></div>');
    const buttonDel = $('<button class="fa fa-trash-o" id="deleteMessage"></button>');
    const buttonEdit = $('<button class="fa fa-pencil" id="editMessage"></button>');
    return div.append(buttonDel, buttonEdit);
  }

  // delete message
  function deleteMessage(data) {
    const $el = $(`#${data.idmessage}`);
    $el.removeClass();
    $el.addClass('deleted');
    $el.html(`message deleted by ${data.username}`);
  }
  // edit message
  function editMessage(data) {
    const $spanEdit = $('<span class="messageEdit">edited</span>');
    const newDate = moment(data.newDate).local().format('H:mm');
    $(`#${data.idmessage} .messageBody`).text(data.newMessage);
    if ($(`#${data.idmessage} .messageEdit`).length === 0) {
      $(`#${data.idmessage} .messageStatus`).append($spanEdit);
    }
    $(`#${data.idmessage} .messageDate`).text(newDate);
  }

  // Whenever user approve changed message
  function approveEditing() {
    const newMessage = $inputMessage.val();
    const newDate = moment.utc();
    editMessage({
      idmessage,
      newMessage,
      newDate
    });
    socket.emit('edit message', {
      idmessage,
      newMessage,
      edited: true,
      newDate
    });
    $inputMessage.val('');
  }

  // Whenewer the user clicks on login of other user, add this login to
  // inputMessage
  function setLoginInInput(login) {
    $inputMessage.val(`${login}, `);
  }

  // Keyboard events

  $window.keydown((event) => {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (editing) {
        approveEditing();
        editing = false;
        return;
      }
      if (username) {
        sendMessage();
        socket.emit('stop typing');
        typing = false;
      } else {
        setUsername();
      }
    }
  });

  $inputMessage.on('input', () => {
    updateTyping();
  });

  // Click events

  // Focus input when clicking anywhere on login page
  $loginPage.click(() => {
    $currentInput.focus();
  });

  // Focus input when clicking on the message input's border
  $inputMessage.click(() => {
    $inputMessage.focus();
  });

   // show buttons for edit and delete message
  $('.messages').dblclick((e) => {
    const $message = $(e.target).parents('.message');
    if ($message && $('.username', $message).text() === username) {
      if ($('.editMessage', $message).length) return;
      $('.messageWrapper', $message).append(createButtons());
    }
  });

  // Show button "delete" and "edit" on message
  $('.messages').click((e) => {
    if (e.target.id === 'deleteMessage') {
      const sure = confirm('Are you really want to delete this message?');
      if (sure) {
        const $message = $(e.target).parents('.message');
        idmessage = $message.attr('id');

        deleteMessage({
          username,
          idmessage
        });
        socket.emit('delete message', {
          username,
          idmessage
        });
      }
    }
    if (e.target.id === 'editMessage') {
      const $message = $(e.target).parents('.message');
      idmessage = $message.attr('id');
      const $messageBodyText = $(e.target).parents('.messageWrapper')
        .children('.messageContent')
        .children('.messageBody')
        .text();

      $inputMessage.val($messageBodyText);
      $inputMessage.focus();
      editing = true;
    }
  });

  // hide buttons for edit and delete message
  $('body').click((e) => {
    if (e.target.className.indexOf('editMessage') >= 0
      || e.target.className.indexOf('deleteMessage') >= 0
      || e.target.className.indexOf('editMessage') >= 0
      ) { return; }
    $('.editMessage').remove();
  });

   // Event of click on login of user
  $messages.click((e) => {
    if ($(e.target).parents('.messageWrapper').length) {
      const login = $(e.target).parents('.messageWrapper')
        .children('.messageContent')
        .children('.username')
        .text();
      if (login === username) {
        return;
      }
      setLoginInInput(login);
      $inputMessage.focus();
    }
  });

  // Socket events

  // Whenever the server emits 'login', log the login message
  socket.on('login', (data) => {
    connected = true;
    // Display the welcome message
    const message = 'Welcome to Chat – ';
    log(message, {
      prepend: true
    });
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', (data) => {
    addChatMessage(data);
  });

  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', (data) => {
    log(`${data.username} joined`);
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', (data) => {
    log(`${data.username} left`);
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', (data) => {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', (data) => {
    removeChatTyping(data);
  });

  // Load old message when user connected to chat
  socket.on('load old messages', (docs) => {
    for (let i = docs.length - 1; i >= 0; i--) {
      addChatMessage(docs[i]);
    }
  });

  // Delete own message from chat
  socket.on('delete message', (data) => {
    deleteMessage(data);
  });

  // Edit own message from chat
  socket.on('edit message', (data) => {
    editMessage(data);
  });
});
