// Setup basic express server
const express = require('express');

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const port = process.env.PORT || 3000;
const moment = require('moment');
const mongoose = require('mongoose');


server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
mongoose.connect('mongodb://localhost/chatDB', (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Connected to mongodb');
  }
});

app.use(express.static(`${__dirname}/../front_end/public`));

// MongoDB shema and model
const chatSchema = mongoose.Schema({
  username: String,
  message: String,
  messageDate: { type: Date, default: moment.utc() },
  idmessage: String,
  deleted: Boolean,
  edited: Boolean
});
const Chat = mongoose.model('Message', chatSchema);

// Chatroom
let numUsers = 0;

io.on('connection', (socket) => {
  let addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // create new message for DB
    const newMsg = new Chat({
      username: socket.username,
      message: data.message,
      messageDate: data.messageDate,
      idmessage: data.idmessage,
      deleted: data.deleted,
      edited: data.edited
    });
    newMsg.save((err) => {
      if (err) throw err;
      // we tell the client to execute 'new message'
      socket.broadcast.emit('new message', {
        username: socket.username,
        message: data.message,
        messageDate: data.messageDate,
        idmessage: data.idmessage,
        deleted: data.deleted,
        edited: data.edited
      });
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username) => {
    if (addedUser) return;

    // load last 20 messages
    const query = Chat.find({});
    query.sort('-idmessage').limit(20).exec((err, docs) => {
      if (err) throw err;
      socket.emit('load old messages', docs);
    });

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers
    });

    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      --numUsers;

      // echo globally that this client has left
      socket.broadcast.emit('user left', {
        username: socket.username,
        numUsers
      });
    }
  });
  // when the client emits 'delete message', we broadcast it to others
  socket.on('delete message', (data) => {
    Chat.findOne({ idmessage: data.idmessage }, (err, findObj) => {
      if (err) throw err;
      findObj.deleted = true;
      findObj.message = '';
      findObj.messageDate = '';
      findObj.save((err) => {
        if (err) throw err;
        socket.broadcast.emit('delete message', {
          username: data.username,
          idmessage: data.idmessage
        });
      });
    });
  });

  // when the client emits 'edit message', we broadcast it to others
  socket.on('edit message', (data) => {
    Chat.findOne({ idmessage: data.idmessage }, (err, findObj) => {
      if (err) throw err;
      findObj.edited = data.edited;
      findObj.message = data.newMessage;
      findObj.messageDate = data.newDate;
      findObj.save((err) => {
        if (err) throw err;
        socket.broadcast.emit('edit message', {
          idmessage: data.idmessage,
          newMessage: data.newMessage,
          edited: true,
          newDate: data.newDate
        });
      });
    });
  });
});
